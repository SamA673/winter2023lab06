// Samin Ahmed 2043024


import java.util.Random; 

public class Dice {
	private int faceValue;
	private Random random; 
	
	
	public Dice(){
		this.faceValue = 1 ; 
		this.random = new Random(); 
		
		
	}
	
	
	
	public int getFaceValue(){
		return this.faceValue; 
	}
	
	public void roll(){
		this.faceValue = (random.nextInt(6)+1);
		
		
	} 
	
	public String toString(){
		return String.valueOf(this.faceValue); 
	} 


} 