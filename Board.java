// Samin Ahmed 2043024

public class Board{
	private Dice d1; 
	private Dice d2;
	private boolean[] tiles; 
	
	public Board(){
	this.d1 = new Dice();
	this.d2 = new Dice(); 
	this.tiles = new boolean[12]; 
	
	
	
	} 
	
	public String toString(){
		String answer = ""; 
		for (int i = 0; i <tiles.length; i++){
			if(!tiles[i]){
			answer += String.valueOf(i+1);
			// answer += ""+(i+1); and answer+= Integer.toString(i+1) both work!!!
			} else {
			answer += "X";
			} 
		} 
	
	 return answer; 
	} 
	
	
	public boolean playATurn(){
		// Roll Die, Print output & save Sum
		d1.roll(); 
		d2.roll(); 

		System.out.println("              Your 1st dice rolled " + d1); 
		System.out.println("              Your 2nd dice rolled " + d2); 
		
		int sumOfDice = d1.getFaceValue() + d2.getFaceValue(); 
		
		
		// Sum index is open...
		if (!tiles[sumOfDice - 1]){
			tiles[sumOfDice - 1] = true; 
			System.out.println("	   Closing tile equal to sum: " + sumOfDice + '\n' ); 
			return false; 
			
		// D1 is open 
		}  else if (!tiles[d1.getFaceValue() - 1 ] ){
				tiles[d1.getFaceValue() - 1] = true; 
				System.out.println("            Closing 1st dice value: " + d1.getFaceValue() + '\n' ); 
				return false; 
			} else if (!tiles[d2.getFaceValue() - 1]){
				tiles[d2.getFaceValue() - 1] = true;
				System.out.println("            Closing 2nd dice value: " + d2.getFaceValue() + '\n'); 
				return false;
			}
			
		
	
		
		// All possibilities are closed... 
		System.out.println('\n' + "  All the tiles for these values are already shut!"); 
		return true;
		
	} 


} 