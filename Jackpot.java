// Samin Ahmed 2043024

import java.util.Scanner; 


public class Jackpot{
	public static void main(String[] args){
		String playerAnswer;
		Scanner scan = new Scanner (System.in); 
		boolean wannaPlay = true;
		int winCount = 0; 
		
		// Play 
		System.out.println("                Welcome to jackpot!");
		for(int i = 1; wannaPlay; i++){
			System.out.println("----------------------Game " + i +  "----------------------"); 
			Board gameBoard = new Board();  
			boolean gameOver = false; 
			int numOfTilesClosed = 0; 

		
			// Play moves
			while(!gameOver){
				System.out.println("                 " + gameBoard + '\n'); 
			
				if(gameBoard.playATurn()){
				gameOver = true; 
				} else {
				numOfTilesClosed++; 
				}
			}

			// Checking win / loss 
			if (numOfTilesClosed >= 7){
			System.out.println("           You hit the jackpot and won!");
			winCount++; 
		
			}  else {
			System.out.println("                    You lose :("); 
			}
		
			// Play again? 
			System.out.println("--------------------------------------------------"); 
			System.out.println("             Play again? \"yes\" or \"no\" ");
			System.out.print("                        "); 
			playerAnswer = scan.nextLine();
		
			// Player does not want to play more
			if (playerAnswer.equals("no")){
				wannaPlay = false;
				System.out.println("          You won a total of " + winCount + " time(s)!"); 
				System.out.println("        Thanks for playing! Come again :)");
			
			// Player wants to play more
			} else if (playerAnswer.equals("yes")){
		
			// We don't know what the player wants... end game. 
			} else{
				System.out.println('\n' + "I don't know what \"" + playerAnswer + "\" means... " + '\n' + "I'll take that as a no."); 
				System.out.println("Oh and by the way... you won " + winCount + " time(s)."); 
				wannaPlay = false; 
			}
		
		} 
		

	}
}	

 